<?
include 'header.php';

?>
<center>
<div style="background: rgba(255,255,255,0.8); border-radius: 10px;">
<h3>How to earn shop points</h3>
There are multiple ways for a person to earn points. One example would be, inviting your friends.<br>
When you invite friends, you both earn 15 points to use on the shop. Check 'Friends' section of your accoutn for more information.
 Another way to earn points would be to talk in Discussion section. Once your friends upvote your posts, you earn points. If they downvote them, you lose points.
 So make sure to post interesting posts! A third way to earn points, would be to create your own group. 
 Once you create a group, any people who enter it, will earn you points. Points will be lost if they leave. 
 So make sure to tempt them to stay active!<br>
 <br />
  </center>
 <br>
 <div style="background: rgba(255,255,255,0.8); border-radius: 10px; margin-left: auto; margin-right: auto; width: 450px;">
 <div style="margin-left: auto; margin-right: auto; width: 300px;">
 Ways to earn:<br>
 <li>Invite Friends - 15 Pts per accepted request</li>
 <li>Post in discussions - 2 Pts per upvote</li>
 <li>Make a group - 2 Pts per member</li>
 </div>
 </div>
 </div>