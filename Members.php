<?
include 'header.php';
if ($EnableAllMembersPage == TRUE){
session_start();
header("Cache-control: private");
if ($_SESSION[$SessionToAccess] != $Granted)
include 'NoPermission.php';
else if ($_SESSION[$SessionToAccess] == $Granted){
?>
<br>
<center><h3>All Members</h3></center>
<?
if ($EnableFirstMembers == TRUE){
?>
<ul id=Members>
	<center><h3>First <?echo $HowManyFirstMembers;?> Members</h3></center>
  <table class="table table-striped table-hover">
              <thead>
                <tr>
				  <th>#</th>
                  <th>Username</th>
                  <th>First name</th>
                  <th>Join Date</th>
                  <th>Email</th>
                  <th>Rank</th>
                </tr>
              </thead>
              <tbody>
<?php
for ($num = 0; $num <= $HowManyFirstMembers; $num++) {
if ($num == 0)
$row = Query2("SELECT * FROM Users ORDER BY ID ASC LIMIT 1");
else
$row = Query2("SELECT * FROM Users ORDER BY ID ASC LIMIT ".$num.",".$num);
if($row[Username] != null){
echo "<tr>";
echo "<td>".$row[ID]."</td>";
echo "<td><a href='User?u=".$row[ID]."'>".$row[Username]."</a></td>";
echo "<td>".$row[FirstName]."</td>";
$Dated = explode(" ", $row[Date]);
echo "<td>".$Dated[0]."</td>";
echo "<td><a href='MAILTO:".$row[Email]."'>".$row[Email]."</a></td>";
$Level = $row[Rank];
if ($Level == 3){
$Level = "Owner";
}
else if ($Level == 2){
$Level = "Administrator";
}
else if ($Level == 1){
$Level = "Moderator";
}
else if ($Level == null){
$Level = "Member";
}
echo "<td>".$Level."</td>";
echo "</tr>";
}
}
?>
</table>
</ul>
<?
}

if ($EnableLatestMembers == TRUE){
?>
<ul id=RMembers>
<center><h3>Latest <? echo $HowManyLatestMembers; ?> Members</h3></center>
  <table class="table table-striped table-hover">
              <thead>
                <tr>
				  <th>#</th>
                  <th>Username</th>
                  <th>First name</th>
                  <th>Join Date</th>
                  <th>Email</th>
                  <th>Rank</th>
                </tr>
              </thead>
              <tbody>
<?php
for ($num = 0; $num <= $HowManyLatestMembers; $num++) {
if ($num == 0)
$row = Query2("SELECT * FROM Users ORDER BY ID DESC LIMIT 1");
else
$row = Query2("SELECT * FROM Users ORDER BY ID DESC LIMIT ".$num.",".$num);
if($row[Username] != null){
echo "<tr>";
$numed = $num + 1;
echo "<td>".$numed."</td>";
echo "<td><a href='User?u='".$row[ID].">".$row[Username]."</a></td>";
echo "<td>".$row[FirstName]."</td>";
$Dated = explode(" ", $row[Date]);
echo "<td>".$Dated[0]."</td>";
echo "<td><a href='MAILTO:".$row[Email]."'>".$row[Email]."</a></td>";
$Level = $row[Rank];
if ($Level == 3){
$Level = "Owner";
}
else if ($Level == 2){
$Level = "Administrator";
}
else if ($Level == 1){
$Level = "Moderator";
}
else if ($Level == null){
$Level = "Member";
}
echo "<td>".$Level."</td>";
echo "</tr>";
}

}
?>
</table>
</ul>
<?
}
}
else
include 'DisabledPage.php';
}
?>