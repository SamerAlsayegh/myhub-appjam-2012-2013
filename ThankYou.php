<?
include 'header.php';
function PopOver($header, $text, $name){
echo '
<script>  
$(function ()  
{ $("#PopOver'.$name.'").popover();  
});  
</script>

<a href="#" id="PopOver'.$name.'" rel="popover" 
data-content="I thank '.$name.' for '.$text.'" 
data-original-title="'.$header.'">'.$name.'</a><br>
';
}
?>
<center>
	<div style="background: rgba(255,255,255,0.8); border-radius: 10px;">
<h5>Website made by: Samer Alsayegh</h5>
<h4>I thank the following individuals/systems who have helped me accomplish this project.</h4>
<h3>Contribution List</h3>
<br>
Thank you to the following people, for their help in making this website what it is, now.<br>
<br>
<br>
..:: Graphics Designer ::..<br>
<?
PopOver("Graphics Designer", "helping with Logo, and Comment buttons used on the website", "Elias_Farah");
PopOver("Background Graphics", "for providing nice and clean CSS Code for the preset backgrounds. Found at: http://lea.verou.me/css3patterns/", "Lea_Verou")
?>
<br>
..:: Systems ::..<br>
<?
PopOver("System Integration", "allowing us to use their Cascading StyleSheets and JavaScript files to help make pages more user friendly. Found at http://twitter.github.io/bootstrap/index.html", "BootStrap");
PopOver("System Integration", "allowing us to use their Editor, used when making a post. Found at http://ckeditor.com", "CKEditor");
?>
</div>
</center>