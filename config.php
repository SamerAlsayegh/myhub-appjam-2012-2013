<?
/*
 *                            		   Suggested Editors: Aptana/NotePad++             
 *                                 		  ....:::Settings:::....
 * 
 *                                 			'D' Means Default Value
 *                                 If D = True Then the accepted variables are only
 *                                               TRUE / FALSE
 *                                 If D = Number Then the accepted variables are only
 *                                                | Integers | Numbers
 */                                          
// The Community Variables
$CommunityTitle = "ExampleCommunity";// Community Title
$EnableTextMenuTitle = TRUE;// Print the Community Title in text for and place as logo.
$Website = "http://MyHub.Example.com";// Website URL | Remove all trailing slashes | Make sure it begins with http://
$LengthOfNormalSession = 60;// Accepted Variables: Integer | In minutes to save the user session for, until they are logged out.
$SupportEmail = "Support@Example.com";// Email for support for the community. Will be mentioned in emails going out.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Page Settings...
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// News Page Settings
$EnableNewsTopics = TRUE;// Accepted Variables: TRUE/FALSE
$NewsTopicsToView = 5;// D = 5 | How many News Topics should be viewed?

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Member's Page Setting | Recommened to be disabled to help private user's account. But is it optional.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$EnableAllMembersPage = FALSE;// D = FALSE | If True, then the Member Page will be enabled. If this one is Disabled then page will be disabled.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If MembersPage is enabled fill these settings
$EnableFirstMembers = TRUE;// D = TRUE | Enable First Members List?
$HowManyFirstMembers = 25;// D = 25 | How many should First Members List Display?
$EnableLatestMembers = TRUE;// D = TRUE | Enable Latest Members List?
$HowManyLatestMembers = 25;// D = 25 | How many should Latest Members List Display?
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Comments/Replies to Topics
$Comments = 5;// D = 5 | Amount of comments to view per page, in the News Topics.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$Pms = 5;// D = 5 | Amount of Private Messages to view per page, in the Messaging Section.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$Discussionss = 5;// D = 20 | Amount of discussions to show the user on the page.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PENDING INVITES
$PendingInvites = 5;// D = 5 | Keep this low. This is the amount of invites a person can make, which are not accepted at a time.
// PendingInvites helps avoid both getting your mail server blocked and avoiding anonymous spam attacks from your server.
// Database Settings | Make sure to use the Database Dump found with these settings.
$Host = "********";// The Host, if database is hosted on Local system, use 'LocalHost'
$User = "********";// The Username to use to access Database.
$Pass = "********";// The Password to use when accessing the Database
$Table = "********";// The Table name for the Database.
/*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 The Email Settings should be set-up by a Network admin or someone knowledgable in the field.
                                            ..:: Details ::...
 Requirements: Have PEAR Mail installed on system | Make sure Mail.php is also set-up correctly in php.ini
 Suggestion: Use Mandrill for Mass Mail, so the mail server is not also affecting system resources.
 They provide free 12,000 Emails each month.   Website: MandrillApp.com

/*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$Mail = "********";// Email to show user that the message came from
$Mail_Host = "********";// SMTP Host | If the Mail server is local, then type 'Localhost'
$Mail_Port = "***";// SMTP Port | Depends on the set-up. Ask Network Admin for this value.
$Mail_Username = "********";// SMTP Username | Username of the Email account to use
$Mail_Password = "********";// SMTP Password | Password of the Email account to use
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>