<?
include 'header.php';
$userid = urldecode($_GET['u']);
?>
<style type="text/css">
#profileinfo{
  box-shadow: 3px 3px 3px 5px #C2C2C2;
  width: 25%;
  top: 25%;
  left: 30%;
  height: auto;
  position:absolute;
  background: rgba(255,255,255,0.8);
  -moz-border-radius: 7px;
  border-radius: 7px;
}
#profileinfoText{
  width: 90%;
  background: rgba(255,255,255,0.8);
  margin-left: auto;
  margin-right: auto; 
}
#profilepic{
  position:absolute;
  background: rgba(255,255,255,0.8);
  top: 17%;
  left: 0%;
  width: auto;
  height: auto;
  max-width: 20%;
  max-height: 20%;
}
#topicsbyuser{
  position:absolute;
  top: 20%;
  left: 65%;
  max-height: 225px;
  box-shadow: 3px 3px 3px 5px #C2C2C2;
  background: rgba(255,255,255,0.8);
  width: 30%;
  height: auto;
  -moz-border-radius: 7px;
  border-radius: 7px;
}
#postsbyuser{
  position:absolute;
  top: 59%;
  left: 65%;
  box-shadow: 3px 3px 3px 5px #C2C2C2;
  width: 30%;
  height: auto;
  max-height:250px;
  -moz-border-radius: 7px;
  background: rgba(255,255,255,0.8);
  border-radius: 7px;
}
#UserBtn{
  position:absolute;
  top: 100px;
  left: 65%;
  background: rgba(255,255,255,0.8);
  width: 400px;
  max-height:250px;
}
</style>
	<div style="background: rgba(255,255,255,0.8); border-radius: 10px;">
<?
if ($id != null){
$row = Query2("SELECT * FROM Users WHERE ID = $userid");
$username = $row[Username];
if (($row[ID] != Null) AND $row[Verified] != 0){
$row2 = Query2("SELECT * FROM UserInfo WHERE UserID = $userid ORDER BY ID DESC");
if ($id != $userid){
echo "<div id='ProfileButtons'>";
echo "<ul id='UserBtn'>";
$friendrequest = Query2("SELECT * FROM Friends WHERE (Requester = $id AND Requested = $userid AND Friends = '1') OR (Requester = $userid AND Requested = $id AND Friends = '1')");
$friendrequested = Query2("SELECT * FROM Friends WHERE Requester = $id AND Requested = $userid AND Friends = '0' AND RequestTime != '0000-00-00 00:00:00'");
if ($friendrequested[ID] != null)
echo "<a class='btn btn-success'>Awaiting Response...</a> ";

else if ($friendrequest[ID] == null){
echo "<a onclick='SendRequest();' class='btn btn-success'>Send Friend Request</a>";
echo '<script type="text/javascript">
function SendRequest() {
    $.ajax({
   type: "POST",
   url: "functions/request.php?f='.$userid.'",
   success: function(){
	$("#ProfileButtons").load(location.href + " #ProfileButtons");
   }
 });
     return false;
}
</script>
';
}
else {
echo "<a href='FriendsHub.php' class='btn btn-danger'>Delete Friend</a> ";
}


echo "<a class='btn btn-inverse' href='PrvM.php?m=$userid'>Send Message</a>";

$perms = Query2("SELECT * FROM Permissions WHERE User = '$id'");
if ($perms[DeleteMembers] == "1"){
echo "<a class='btn btn-danger' type='submit' onclick='DeleteUser();' >Delete User</a></center>";
echo '<script type="text/javascript">
function DeleteUser() {
    $.ajax({
   type: "POST",
   url: "functions/User.php?U='.$userid.'&type=d",
   success: function(){
	window.location.replace("index.php");
   }
 });
     return false;
}
</script>
';

}


echo "</ul>";
echo "</div>";
}

echo "<center>";
echo "<h3>Profile of ".GetUser($row[ID])."</h3>";
echo "</center>";
echo "<ul id='profileinfo'><ul id='profileinfoText'>";
echo "<center><b>Account information</b></center>";
echo "<hr>";
echo "User ID: ".$row[ID];
echo "<br>";
echo "Full Name: ".$row[FirstName]." ".$row[LastName];
echo "<hr>";
echo "<center><b>Sign-up Information</b></center>";
echo "<hr>";
echo "Registered ".GetTheRealTime($row[Date]);
echo "<br>";
echo "Last active ".GetTheRealTime($row2[LastLogin]);
echo "</ul></ul>";
echo "<ul id='profilepic'>";
echo "<img src='".$row[ImgLink]."'/>";
if ($id == $userid)
echo "<center><a href='Profile.php'>Edit Profile</a>";
echo "</ul>";

echo "<ul id='topicsbyuser'>";
echo "<center><b>Topics by ".$username."</b></center>";
for ($a = 0; $a <= 9; $a++){
if ($a == 0)
$row = Query2("SELECT * FROM Topics WHERE Author = $userid LIMIT 1");
else
$row = Query2("SELECT * FROM Topics WHERE Author = $userid LIMIT ".$a.",".$a);
if ($row[PostTitle] != null)
echo "- <a href='Posting.php?Topic=".$row[ID]."'>".$row[PostTitle]."</a><br>";
}
echo "</ul>";

echo "<ul id='postsbyuser'>";
echo "<center><b>Posts by ".$username."</b></center>";
for ($a = 0; $a <= 10; $a++){
if ($a == 0){
$row = Query2("SELECT * FROM Posts WHERE Author = $userid ORDER BY ID DESC LIMIT 1");
$row1 = Query2("SELECT * FROM Topics WHERE ID = ".$row[Topic]." ORDER BY ID DESC");
}
else{
$row = Query2("SELECT * FROM Posts WHERE Author = $userid ORDER BY ID DESC LIMIT ".$a.",".$a);
$row1 = Query2("SELECT * FROM Topics WHERE ID = ".$row[Topic]." ORDER BY ID DESC");
}
if (strlen($row[Comment]) >= 40){
$Comment = substr($row[Comment], 0, 25). " ... " . substr($row[Comment], -5);
}
else
$Comment = $row[Comment];
if ($row[Topic] != null)
echo "- <a href='Posting.php?Topic=".$row[Topic]."'><font style='opacity=0.4;'>".$Comment."</font></a><br>";
}
echo "</ul>";
}
else{
	echo "<center><b>";
	echo "This user does not exist or has not activated their account yet, make sure to have used a valid link.";
	echo "</b></center>";
}
}
else 
include 'NoPermission.php';
?>
</div>