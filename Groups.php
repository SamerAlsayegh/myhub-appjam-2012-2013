<?
include 'header.php';
?>
	<div style="background: rgba(255,255,255,0.8); border-radius: 10px;">
<center><h2><font style="text-shadow: 0px 0px 5px white;">Groups</font></h2>
<br>
<?
if ($id != null){
?>
<a class="btn btn" href="#CreateGroup" data-toggle="modal">Create a Group</a>
</div>
<div id="CreateGroup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
<h3 id="myModalLabel"><center><h3>Create a Group!</h3></center></h3>
</div>
<center>
<div class="modal-body">

<script>
function CheckGroupFields()
{
  //Registrtion form
  var a=document.forms["CGroup"]["Gname"].value;
  var b=document.forms["CGroup"]["GDetails"].value;
  var b=document.forms["CGroup"]["Catagory"].value;
  
  if ((a.length == 0) || (b.length == 0)){
	alert("Make sure to fill out all fields.");
	return false;
	}
	if (b == "What is your group about?"){
	alert("You must fill out the details accurately. Or your page may be taken down.");
	return false;
	}
	if (c.length == 0){
	alert("You must specifiy what catagory your group fits under.");
	return false;
	}
	
	
}
</script>
<form name="CGroup" id="CGroup" onsubmit="return CheckGroupFields();" action="functions/MakeGroup.php" method="post">
<center><b>Enter your group details, so that it can be set-up! As easy as 1, 2, 3!</b></center><br>
  <input type="text" name="Gname" id="Gname" placeholder="Group Name"/>
  <br />
  <textarea name="GDetails" style="width: 300px; max-width: 75%; height: 100px;" id="GDetails" placeholder="What is your group about? (Max 2000 characters)"></textarea>
  <br />
<select name="Catagory">
<option value="" selected="selected">Choose a Group Catagory</option>
<option value="1">Gaming</option>
<option value="2">Entertainment</option>
<option value="3">Lifestyle</option>
<option value="4">Internet</option>
<option value="7">Product</option>
<option value="8">General</option>
<option value="9">Other</option>
</select>
  <br />
  <input class="btn btn-inverse" type="submit" name="submit" value="Create Group" />
</form>
    </center>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>
</div>
</div>
</center>
            <table style="background: rgba(255,255,255,0.8); border-radius: 10px;" class="table table-hover">
              <thead>
                <tr>
                  <th>Group Name</th>
                  <th>Date of Publishing</th>
                  <th>Manager</th>
                  <th>Catagory</th>
                  <th>Members</th>
                </tr>
              </thead>
              <tbody>
<?
for ($a = 0; $a <= 40; $a++){
if ($a == 0)
$row = Query2("SELECT * FROM Groups ORDER BY ID ASC LIMIT 1");
else
$row = Query2("SELECT * FROM Groups ORDER BY ID ASC LIMIT ".$a.", ".$a);

if ($row != null){
echo "<tr>";
echo "<td>";
echo "<a href='Group?g=".$row[ID]."'>".$row[Name]."</a>";
echo "</td>";
echo "<td>";
echo GetTheRealTime($row[Creation]);
echo "</td>";
echo "<td>";
$owner = Query2("SELECT * FROM Users WHERE ID = $row[Owner] LIMIT 1");
echo "<a href='User?u=$owner[ID]'>".GetUserName($owner[ID])."</a>";
echo "</td>";
echo "<td>";
echo PageCatagory($row[Catagory]);
echo "</td>";

$TotalMembers = Query2("SELECT *, Count(*) AS Members FROM GroupJoins WHERE GroupID = '".$row[ID]."' GROUP BY GroupID");
echo "<td>";
echo $TotalMembers[Members];
echo "</td>";
echo "</tr>";
}
}
?>
              </tbody>
            </table>
            
<?
}
?>