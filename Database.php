<?
include 'config.php';
$TimeToRefreshComments = 5; // Kept here. Does not work | But kept incase of any dependancies.
$server = mysql_connect($Host, $User, $Pass);
if (!$server) die(mysql_error());
mysql_select_db($Table);

$CommunityName = $CommunityTitle;
// Some functions which will be used dynamically throughout the code. Making the code much smaller than it is.
// This function is made so that it just runs a query on the Database, without retrieval of any values.
function Query($query){
$result = mysql_query($query);
return $result;
}
// This function is made so that it returns a value from the database.
function Query2($query){
$result1 = mysql_query($query);
$row = mysql_fetch_array($result1);
return $row;
}
function GetUser ($ID){
$PersonName = Query2("SELECT * FROM Users WHERE ID = '$ID'");
$NAME = $PersonName[FirstName]." ".$PersonName[LastName];
return $NAME;
}
function GetUserName ($ID){
$PersonName = Query2("SELECT * FROM Users WHERE ID = '$ID'");
$Rewards = Query2("SELECT * FROM Rewards WHERE User = $ID");

if ($Rewards[Used] != null){

if (strpos($Rewards[Used],'Bend'))
$Italic = true;
if (strpos($Rewards[Used],'Bold'))
$Bold = true;
if (strpos($Rewards[Used],'Fontasion'))
$Fontasion = true;
if (strpos($Rewards[Used],'Blinker'))
$Blinker = true;
if (strpos($Rewards[Used],'Underline'))
$Underline = true;
if (strpos($Rewards[Used],'Overline'))
$Overline = true;
?>
<font style='<? 
if ($Italic == true)
echo "font-style:italic;";
if ($Bold == true)
echo "font-weight: bold;";
if ($Fontasion == true)
echo "text-shadow: 0px 0px 1px black;";
if ($Blinker == true)
echo "text-decoration:blink;";
else if ($Underline == true)
echo "text-decoration:underline;";
else if ($Overline == true)
echo "text-decoration:overline;";
?>'>
<?
echo $PersonName[FirstName]." ".$PersonName[LastName];
echo "</font>";
}
else
$NAME = $PersonName[FirstName]." ".$PersonName[LastName];



return $NAME;
}
/* 
 * Pretty big function, to make dates on the website more friendly.
 * TIME() could have been used on the database to make the database slightly smaller
 * But as it is, wont make much of a difference, since we want to make the application
 * Very client and administrator friendly.
 * 
 */
function GetTheRealTime ( $Date ){
$brokenDate = explode(" ", $Date);
// Actual Date in Year-Month-Day
$Realdate = $brokenDate[0];
// Actual Time in Hours:Minutes:Seconds
$Realtime = explode(":",$brokenDate[1]);

// We should then just get the time of posting since its the same day.

// We break up the time, using Explode
$realTimeHour = $Realtime[0];
$realTimeMinute = $Realtime[1];
$realTimeSecond = $Realtime[2];

// We will break up RealDate
$RealdateV2 = explode("-", $Realdate);
$Day = $RealdateV2[2];
$Month = $RealdateV2[1];
$Year = $RealdateV2[0];


// We check to see if the time is over 12 or not, so we can display AM or PM
if (date("Y-m-d") == $Realdate){

if ($PostRealTime == TRUE){
	if ($realTimeHour > 12)
{
$StatusOfDay = "PM";
$realTimeHour = $realTimeHour - 12;
}
else
$StatusOfDay = "AM";

$PostDateTime = "Today at ".$realTimeHour.":".$realTimeMinute." ".$StatusOfDay." EDT";
}
else{
$PostDateTimeH = date("H") - $realTimeHour;
$PostDateTimeM = date("i") - $realTimeMinute;
$PostDateTimeS = date("s") - $realTimeSecond;


if ($PostDateTimeM == 0)
$PostDateTime = " a couple seconds ago";
else if ($PostDateTimeH == 0){
if ($PostDateTimeM == 1)
$PostDateTime = $PostDateTimeM." minute ago";
else
$PostDateTime = $PostDateTimeM." minutes ago";
}
else if ($PostDateTimeH > 0){
if ($PostDateTimeH == 1)
$PostDateTime = $PostDateTimeH." hour ago";
else
$PostDateTime = $PostDateTimeH." hours ago";
}
}
}
else{
// We apply fixes in the dates to make the dates more "Eye friendly"
if (strpos($Day,'21'))
$Transition = "st";
else if (strpos($Day,'22 '))
$Transition = "nd";
else if (strpos($Day,'23 '))
$Transition = "rd";
else if (substr($Day, 1, 1) == '1 ')
$Transition = "st";
else if (substr($Day, 1, 1) == '2 ')
$Transition = "nd";
else if (substr($Day, 1, 1) == '3 ')
$Transition = "rd";
else
$Transition = "th";

if(substr($Day, 0, 1) == '0')
$Day = substr($Day, 1);

if(substr($Month, 0, 1) == '0')
$Month = substr($Month, 1);

/* 
 * 
 * If the viewing of the post is not the same day as it was posted, we will show a more suitable date instead.
 * Try changing dates on a MYSQL Database to expirement the different dates.
 * It will apply the correct suffix for the day. For example: 1ST / 2ND / 3RD / 4TH and so on...
 * 
 */
if ((date("m") == $Month) && (date("Y") == $Year))
$Dates = "the ".$Day.$Transition." this month";
// If its not the same month and year, 
else if (date("Y") == $Year)
$Dates = date("M", mktime(0, 0, 0, $Month))." ".$Day.$Transition;

// If we are in a different year, then it will display the date in a friendly format.
else if (date("Y") != $Year)
$Dates = $Day."/".$Month."/".$Year;


if ($realTimeHour > 12)
{
$StatusOfDay = "PM";
$realTimeHour = $realTimeHour - 12;
}
else
$StatusOfDay = "AM";


if ($realTimeHour == null)
$PostDateTime = " Deleted.";
else if ($Dates != null)
$PostDateTime = "at ".$realTimeHour.":".$realTimeMinute." ".$StatusOfDay." EDT on ".$Dates;
}

return $PostDateTime;
}


// If we need to redirect users, we will use this one to also display some values using alerts. Ex You have been logged out, Invalid login, ect..
function Redirect($id, $value){
echo "<meta http-equiv='refresh' content='0; url=index.php?".$id."=".$value."'>";
}
// If we need to redirect users, we will use this to take them back to previous page.
function RedirectB(){
	echo "<meta http-equiv='refresh' content='0; url=".$_SERVER["HTTP_REFERER"]."'>";
}
function RedirectIndex(){
	echo "<meta http-equiv='refresh' content='0; url=../index.php'>";
}
function RedirectIndexR(){
	echo "<meta http-equiv='refresh' content='0; url=index.php'>";
}
function PageCatagory( $CatagoryID ) {
	If($CatagoryID == "1")
	$Name = "Gaming";
	Else If($CatagoryID == "2")
	$Name = "Entertainment";
	Else If($CatagoryID == "3")
	$Name = "Lifestyle";
	Else If($CatagoryID == "4")
	$Name = "Internet";
	Else If($CatagoryID == "5")
	$Name = "Class";
	Else If($CatagoryID == "6")
	$Name = "Project";
	Else If($CatagoryID == "7")
	$Name = "Product";
	Else If($CatagoryID == "8")
	$Name = "General";
	Else If($CatagoryID == "9")
	$Name = "Other";
	Else
	$Name = "Invalid";
	return $Name;
}

function SendMail ($email, $text, $type){
 require_once "Mail.php";
 include 'config.php';
 $from = $CommunityTitle." Community <".$Mail.">";
 $to = $email;
 $subject = $type;
 $body = $text;
 
$headers = array ('MIME-Version' => '1.0',
        'Content-Type' => "text/html; charset=ISO-8859-1",
        'From' => $from,
        'To' => $to,
        'Subject' => $subject
     );
 $smtp = Mail::factory('smtp',
   array ('host' => $Mail_Host,
     'port' => $Mail_Port,
     'auth' => true,
     'username' => $Mail_Username,
     'password' => $Mail_Password));
 $mail = $smtp->send($to, $headers, $body);
}
$UsrId = $_COOKIE["LG"];
// Some information we could really use in other files. To validate user's and sort of actions.
if ($UsrId != null){
$row = Query2("SELECT * FROM Users WHERE ShaUsr = '".$UsrId."'");
if ($row[ID] != null)
$id = $row[ID];
else
$id = null;
}
else{}
/*
$SessionToAccessTime = $CommunityName."SESSION"."$row[SESSION]";
$SessionToAccess = $CommunityName."SESSION".sha1($row[Username]);
$SessionToAccessName = $Granted;
 * I used sessions before, but because of their unreliability, as they may cause some issues some times, I began using Cookies, which compare values with the database.
*/
include 'bootstrap.php';
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-30129833-3', 'pvp-hg.com');
  ga('send', 'pageview');

</script>
<script type="text/javascript" src="editor/ckeditor.js"></script>